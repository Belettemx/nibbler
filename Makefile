# ---------------------------------------------------------------------------- #
# PROJECT DATA
# ---------------------------------------------------------------------------- #

NAME		=	Nibbler

# ---------------------------------------------------------------------------- #

SRCS		=	main.cpp				\
				Parser.cpp				\
				AppleHandler.cpp 		\
				AppleObject.cpp 		\
				GameHandler.cpp 		\
				Point.cpp 				\
				Timer.cpp 				\
				Score.cpp 				\
				SnakeHandler.cpp 		\
				SnakeObject.cpp 		\
				Displayer_factory.cpp
# ---------------------------------------------------------------------------- #
# PROJECT CONFIGURATION
# ---------------------------------------------------------------------------- #

ROOT_DIR 	= `pwd`

CFLAGS		=	\
				-Wall -Wextra -Werror			\

# >>> REQUIRED FOR LIBRARIES >>> THINK ABOUT CHANGING THE *LIBS rules

CPPFLAGS	=	\
				-I $(DIRINC)					\

LDFLAGS		=	\

LDLIBS		=	\

# GLOBAL SETUP
AR			=	ar
CC			=	clang++
RM			=	rm
MKDIR		=	mkdir
MAKE		=	make

DIRSRC		=	./srcs/
DIROBJ		=	./.objs/
DIRINC		=	./incs/
DIRLIB		=	./libs/

# EXTRA COLOR AND DISPLAY FEATURE DE OUF
C_DFL		=	\033[0m
C_GRA		=	\033[30m
C_RED		=	\033[31m
C_GRE		=	\033[32m
C_YEL		=	\033[33m
C_BLUE		=	\033[34m
C_MAG		=	\033[35m
C_CYA		=	\033[36m
C_WHI		=	\033[37m

# ============================================================================ #

# ---------------------------------------------------------------------------- #
# SOURCES NORMALIZATION
# ---------------------------------------------------------------------------- #

SRC			=	$(addprefix $(DIRSRC), $(SRCS))
OBJ			=	$(addprefix $(DIROBJ), $(notdir $(SRC:.cpp=.o)))

# ---------------------------------------------------------------------------- #
# RULES
# ---------------------------------------------------------------------------- #

all			:	$(NAME)
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) build completed\n" "$(MAKE)"

$(NAME)		:	$(DIROBJ) $(OBJ) libs
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) linking objects\n" "$(CC)"
	@$(CC) $(OBJ) -o $(NAME) $(LDFLAGS) $(LDLIBS)

# ---------------------------------------------------------------------------- #
# CUSTOMISABLE RULES

libs		:
	@make -C ./lib/ncurses/
	@make -C ./lib/sdl/
	@make -C ./lib/SFML/
	@install_name_tool -change \
		/usr/local/lib/libSDL2-2.0.0.dylib \
		$(ROOT_DIR)/lib/sdl/lib/libSDL2.dylib \
		lib/sdl.so
	@install_name_tool -change \
		@rpath/libsfml-window.2.3.dylib \
		$(ROOT_DIR)/lib/SFML/SFML/lib/libsfml-window.2.3.dylib \
		lib/sfml.so
	@install_name_tool -change \
		@rpath/libsfml-system.2.3.dylib \
		$(ROOT_DIR)/lib/SFML/SFML/lib/libsfml-system.2.3.dylib \
		lib/sfml.so
	@install_name_tool -change \
		@rpath/libsfml-graphics.2.3.dylib \
		$(ROOT_DIR)/lib/SFML/SFML/lib/libsfml-graphics.2.3.dylib \
		lib/sfml.so
	@install_name_tool -change \
		@rpath/../Frameworks/freetype.framework/Versions/A/freetype \
		$(ROOT_DIR)/lib/SFML/SFML/extlibs/freetype.framework/Versions/A/freetype \
		$(ROOT_DIR)/lib/SFML/SFML/lib/libsfml-graphics.2.3.dylib

cleanlibs	:
	@make -C ./lib/ncurses/ clean
	@make -C ./lib/sdl/ clean
	@make -C ./lib/SFML/ clean

fcleanlibs	: 
	@make -C ./lib/ncurses/ fclean
	@make -C ./lib/sdl/ fclean
	@make -C ./lib/SFML/ fclean

# ---------------------------------------------------------------------------- #

clean		: cleanlibs
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove objects\n" "$(RM)"
	@$(RM) -rf $(DIROBJ)

fclean		:	fcleanlibs clean
	@printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) remove binaries\n" "$(RM)"
	@$(RM) -f $(NAME)

re			:	fclean all

$(DIROBJ)	:
	@$(MKDIR) -p $(DIROBJ)

depend		:
	@sed -e '/^#start/,/^end/d' Makefile > .mftmp && mv .mftmp Makefile
	@printf "#start\n\n" >> Makefile
	@$(foreach s, $(SRC),																\
		printf '$$(DIROBJ)'										>> Makefile	&& \
		$(CC) -MM $(s) $(CPPFLAGS)								>> Makefile	&& \
																			\
		printf '\t\t@printf "$$(C_GRE)[ $(NAME) ] '				>> Makefile && \
		printf '[ %%-8s ]$$(C_DFL) " "$(CC)"\n'					>> Makefile && \
		printf '\t\t@printf "compiling $(s)\\n"\n'				>> Makefile	&& \
																			\
		printf '\t\t@$$(CC) -c $(s) -o '						>> Makefile	&& \
		printf '$(addprefix $(DIROBJ), $(notdir $(s:.cpp=.o))) '>> Makefile	&& \
		printf '$$(CPPFLAGS) $$(CFLAGS)\n\n'					>> Makefile	&& \
																			\
		printf "$(C_GRE)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generated\n"										|| \
																			\
		(sed -e '/^#start/,$$d' Makefile > .mftmp && mv .mftmp Makefile		&& \
		printf "#start\n\n"										>> Makefile	&& \
		printf "$(C_RED)[ $(NAME) ] [ %-8s ]$(C_DFL) " "dep"				&& \
		printf "$(s) rule generation failed\n"								) \
	;)
	@printf "\n#end\n" >> Makefile

.PHONY	:	 libs

# ---------------------------------------------------------------------------- #
# AUTO-GENERATED SECTION - do not modify
# ---------------------------------------------------------------------------- #

#start

$(DIROBJ)main.o: srcs/main.cpp incs/Nibbler.hpp incs/GameHandler.hpp \
  incs/IDisplayer.hpp incs/Point.hpp incs/define.hpp \
  incs/SnakeObject.hpp incs/Displayer_factory.hpp incs/Parser.hpp \
  incs/AppleHandler.hpp incs/AppleObject.hpp incs/SnakeHandler.hpp \
  incs/Score.hpp incs/Timer.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/main.cpp\n"
		@$(CC) -c ./srcs/main.cpp -o ./.objs/main.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)Parser.o: srcs/Parser.cpp incs/Parser.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Parser.cpp\n"
		@$(CC) -c ./srcs/Parser.cpp -o ./.objs/Parser.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)AppleHandler.o: srcs/AppleHandler.cpp incs/AppleHandler.hpp \
  incs/AppleObject.hpp incs/Point.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/AppleHandler.cpp\n"
		@$(CC) -c ./srcs/AppleHandler.cpp -o ./.objs/AppleHandler.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)AppleObject.o: srcs/AppleObject.cpp incs/AppleObject.hpp incs/Point.hpp \
  incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/AppleObject.cpp\n"
		@$(CC) -c ./srcs/AppleObject.cpp -o ./.objs/AppleObject.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)GameHandler.o: srcs/GameHandler.cpp incs/GameHandler.hpp \
  incs/IDisplayer.hpp incs/Point.hpp incs/define.hpp \
  incs/SnakeObject.hpp incs/Displayer_factory.hpp incs/Parser.hpp \
  incs/AppleHandler.hpp incs/AppleObject.hpp incs/SnakeHandler.hpp \
  incs/Score.hpp incs/Timer.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/GameHandler.cpp\n"
		@$(CC) -c ./srcs/GameHandler.cpp -o ./.objs/GameHandler.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)Point.o: srcs/Point.cpp incs/Point.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Point.cpp\n"
		@$(CC) -c ./srcs/Point.cpp -o ./.objs/Point.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)Timer.o: srcs/Timer.cpp incs/Timer.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Timer.cpp\n"
		@$(CC) -c ./srcs/Timer.cpp -o ./.objs/Timer.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)Score.o: srcs/Score.cpp incs/Score.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Score.cpp\n"
		@$(CC) -c ./srcs/Score.cpp -o ./.objs/Score.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)SnakeHandler.o: srcs/SnakeHandler.cpp incs/SnakeHandler.hpp \
  incs/SnakeObject.hpp incs/Point.hpp incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/SnakeHandler.cpp\n"
		@$(CC) -c ./srcs/SnakeHandler.cpp -o ./.objs/SnakeHandler.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)SnakeObject.o: srcs/SnakeObject.cpp incs/SnakeObject.hpp incs/Point.hpp \
  incs/define.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/SnakeObject.cpp\n"
		@$(CC) -c ./srcs/SnakeObject.cpp -o ./.objs/SnakeObject.o $(CPPFLAGS) $(CFLAGS)

$(DIROBJ)Displayer_factory.o: srcs/Displayer_factory.cpp \
  incs/Displayer_factory.hpp incs/../incs/IDisplayer.hpp incs/Point.hpp \
  incs/define.hpp incs/SnakeObject.hpp
		@printf "$(C_GRE)[ Nibbler ] [ %-8s ]$(C_DFL) " "clang++"
		@printf "compiling ./srcs/Displayer_factory.cpp\n"
		@$(CC) -c ./srcs/Displayer_factory.cpp -o ./.objs/Displayer_factory.o $(CPPFLAGS) $(CFLAGS)


#end
