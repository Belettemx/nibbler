#ifndef TIMER_CLASS
#define TIMER_CLASS

#include <define.hpp>
#include <chrono>
#include <sstream>
#include <iomanip>

class Timer
{
private:
	std::chrono::steady_clock::time_point 			_startTime;

public:
					Timer();
					Timer(const Timer & src);
	Timer &			operator=(const Timer & src);
	virtual			~Timer();

	void											start();
	std::string	&									getElapsedTime() const;
	const std::chrono::steady_clock::time_point	&	getStartTime() const;
};

std::ostream &	operator<<(std::ostream & o, Timer const & e);

#endif