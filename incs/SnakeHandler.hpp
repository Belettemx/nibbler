#ifndef SNAKEHANDLER_CLASS
#define SNAKEHANDLER_CLASS

#include <SnakeObject.hpp>
#include <define.hpp>
#include <list>
#include <map>
#include <stdlib.h>

class SnakeHandler
{
private:
	std::list<SnakeObject> 		_snakeBody;


public:
						SnakeHandler();
						SnakeHandler(tInterface & interface);
						SnakeHandler(const SnakeHandler & src);
	SnakeHandler & 		operator=(const SnakeHandler & src);
	virtual 			~SnakeHandler();

	Point &								computeDirection();
	const std::list<SnakeObject> & 		getSnakeBody() const;
	const std::list<Point> & 			getSnakeBody(int) const;
	const SnakeObject & 				getSnakeHead() const;
	bool								isSnakeBody(Point & p);
	void								goForwardGrowing(Point & direction);
	void								removeTail();
	void								updateAsset();
	std::string	computeBodyOrientation(std::list<SnakeObject>::iterator bodyPart);
};

std::ostream &	operator<<(std::ostream & o, SnakeHandler const & e);

#endif