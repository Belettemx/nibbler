#ifndef PARSER
# define PARSER

# include <string>
# include "define.hpp"

void	parse(const char **argv, int argc, tInterface * interface);


class ExceptParser : public std::exception {
			private:
				std::string				_msg;

			public:
										ExceptParser();
										ExceptParser(std::string msg);
										ExceptParser(const ExceptParser & src);
				virtual					~ExceptParser() _NOEXCEPT;
				ExceptParser	&		operator=(const ExceptParser & rhs);
				virtual const char *	what() const throw();
};

std::ostream &						operator<<(std::ostream & o, ExceptParser const & i);

#endif