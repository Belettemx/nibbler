#ifndef DEFINE_HPP
#define DEFINE_HPP

#define DEBUGG 2

#define APPLELIFETIME 20

#include <fstream>

extern std::ofstream _log;

typedef struct sInterface tInterface;

struct sInterface{
	int lib;
	int width;
	int height;
};

#define HEAD_DOWN 			0
#define HEAD_RIGHT			1
#define HEAD_UP 			2
#define HEAD_LEFT 			3


#define ANGLE_RIGHT_DOWN	4
#define ANGLE_RIGHT_UP 		5
#define ANGLE_LEFT_DOWN 	6
#define ANGLE_LEFT_UP 		7
#define BODY_VERTICAL 		8
#define BODY_HORIZONTAL		9

#define TAIL_DOWN			10
#define TAIL_RIGHT 			11
#define TAIL_UP 			12
#define TAIL_LEFT 			13



#endif
