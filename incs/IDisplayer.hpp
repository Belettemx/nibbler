#ifndef IDISPALYER_HPP
#define IDISPALYER_HPP
#include <string>
#include <iostream>
#include <list>
#include "Point.hpp"
#include "SnakeObject.hpp"
#include <map>

enum eKey{
	key_undef,
	key_up,
	key_down,
	key_left,
	key_right,
	key_a,
	key_d,
	key_quit,
	key_resize,
	key_enter,
	key_one,
	key_two,
	key_three,
};




class IDisplayer{

	public:
		virtual ~IDisplayer(){};
		virtual void	init(int height, int width) = 0;
		virtual void	close() = 0;
		virtual void	display_all() = 0;
		virtual void	clear_window() = 0;
		virtual int		display_menu(std::string *menu, int size) = 0;
		virtual void	insert_score(std::string score) = 0;
		virtual void	insert_clock(std::string clock) = 0;
		virtual void	insert_snake(std::list<SnakeObject> snake) = 0;
		virtual void	insert_food(std::list<Point> food) = 0;
		virtual void	insert_walls(std::list<Point> walls) = 0;
		virtual eKey	get_input() const = 0;
};

#endif
