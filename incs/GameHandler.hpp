#ifndef GAME_HANDLER
#define GAME_HANDLER

#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <cmath>
#include <random>
#include <unistd.h>

#include <IDisplayer.hpp>
#include <Displayer_factory.hpp>

#include <define.hpp>
#include <Parser.hpp>
#include <AppleHandler.hpp>
#include <SnakeHandler.hpp>
#include <Score.hpp>
#include <Timer.hpp>

class GameHandler
{
private:
	tInterface										_interface;
	unsigned int 									_appleLifeTime;
	unsigned int 									_appleRandomness;
	float 											_frameLimiter;
	Score 		 									_score;
	Timer 											_timer;
	std::default_random_engine 						_rndGen;

	Displayer_factory								*_graphic;
	AppleHandler 									*_appleHandler;
	SnakeHandler 									*_snakeHandler;

public:

					GameHandler();
					GameHandler(int argc, const char **argv);
					GameHandler(const GameHandler & src);
	virtual			~GameHandler();

	GameHandler &	operator=(const GameHandler & src);

	bool			isGood();
	int 			getWidth() const;
	int 			getHeight() const;
	void			appleSpwaner();
	void			movingUpdate(eKey & keyPressed);
	bool			isOutOfBounds(Point & p);
	bool			collisionChecker();
	Point &			findRandomEmptyLocation();
	void			computeAppleSpwaningRate();
	int				gameMenu();
	void			gameLoop();


	class GameHandlerException : public std::exception
	{
	private:
		std::string		_msg;
		unsigned int 	_line;

	public:
						GameHandlerException();
						GameHandlerException(std::string msg);
						GameHandlerException(std::string msg, unsigned int line);
						GameHandlerException(const GameHandlerException & src);
		virtual 		~GameHandlerException() _NOEXCEPT;
		GameHandlerException &	operator=(const GameHandlerException & rhs);
		virtual const char *	what() const throw();
		
	};
};

std::ostream &						operator<<(std::ostream & o, GameHandler const & e);
std::ostream &						operator<<(std::ostream & o, GameHandler::GameHandlerException const & e);

#endif

/*
	TODO
	
	difficulty
	speed update

*/