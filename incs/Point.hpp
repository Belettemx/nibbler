#ifndef POINT_CLASS
#define POINT_CLASS

#include <iostream>
#include <define.hpp>
#include <ostream>
#include "define.hpp"

class Point
{
protected:
	int		_x;
	int		_y;

public:
							Point();
							Point(const Point & src);
							Point(int x, int y);
	Point &					operator=(const Point & src);
	virtual					~Point();
	const int &	getX() const;
	const int &	getY() const;
	void					setX(int x);
	void					setY(int y);
	bool					operator==(const Point & r);
	bool					operator!=(const Point & r);
	bool					operator<(const Point & r) const;
	Point &					operator+(const Point & r);
	Point &					operator-(const Point & r);

};

std::ostream &	operator<<(std::ostream & o, Point const & e);

#endif
