#ifndef SCORE_CLASS
#define SCORE_CLASS

#include <iostream>
#include <math.h>
#include <array>
#include <define.hpp>

class Score
{
private:
	unsigned int 		_score;

public:
							Score();
							Score(const Score & src);
	virtual					~Score();
	Score &					operator=(const Score & src);
	Score &					operator+=(const int & n);
	Score &					operator++();
	Score					operator++(int);

	const unsigned int &	getScore() const;
	std::string				formatScore(int n);
};

std::ostream &	operator<<(std::ostream & o, Score const & e);

#endif