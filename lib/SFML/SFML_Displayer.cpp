#include "SFML_Displayer.hpp"


SFML_Displayer::SFML_Displayer(){
	key_tab[sf::Keyboard::Key::Left] = key_left;
	key_tab[sf::Keyboard::Key::Right] = key_right;
	key_tab[sf::Keyboard::Key::Up] = key_up;
	key_tab[sf::Keyboard::Key::Down] = key_down;
	key_tab[sf::Keyboard::Key::A] = key_a;
	key_tab[sf::Keyboard::Key::D] = key_d;
	key_tab[sf::Keyboard::Key::Q] = key_quit;
	key_tab[sf::Keyboard::Key::Escape] = key_quit; // escape key
	key_tab[sf::Keyboard::Key::Return] = key_enter;
	key_tab[sf::Keyboard::Key::Num1] = key_one;
	key_tab[sf::Keyboard::Key::Num2] = key_two;
	key_tab[sf::Keyboard::Key::Num3] = key_three;

}

SFML_Displayer::~SFML_Displayer(){
}

SFML_Displayer::SFML_Displayer(SFML_Displayer const & src){
	load_background();
	load_snake_heads();
	load_snake_tails();
	load_snake_body();
	load_numbers();
	load_score();
	key_tab = src.get_key_tab();
	_height = src.get_height();
	_width = src.get_width();
	_old_snake = src.get_old_snake();
	_old_food = src.get_old_food();
	_window_size_error = src.get_window_size_error();
	_window = src.get_window();
	_food = src.get_food();
	_walls = src.get_walls();
	_background = src.get_background();
	_menu = src.get_menu();
}

SFML_Displayer	&SFML_Displayer::operator=(const SFML_Displayer & rhs){
	load_background();
	load_snake_heads();
	load_snake_tails();
	load_snake_body();
	load_numbers();
	load_score();
	key_tab = rhs.get_key_tab();
	_height = rhs.get_height();
	_width = rhs.get_width();
	_old_snake = rhs.get_old_snake();
	_old_food = rhs.get_old_food();
	_window_size_error = rhs.get_window_size_error();
	_window = rhs.get_window();
	_food = rhs.get_food();
	_walls = rhs.get_walls();
	_background = rhs.get_background();
	_menu = rhs.get_menu();
	return *this;
}

std::map <int, eKey>		SFML_Displayer::get_key_tab() const{
	return key_tab;
}
size_t						SFML_Displayer::get_height() const{
	return _height;
}
size_t						SFML_Displayer::get_width() const{
	return _width;
}
std::list<Point>			SFML_Displayer::get_old_snake() const{
	return _old_snake;
}
std::list<Point>			SFML_Displayer::get_old_food() const{
	return _old_food;
}
std::string					SFML_Displayer::get_window_size_error() const{
	return _window_size_error;
}
sf::RenderWindow			*SFML_Displayer::get_window() const{
	return _window;
}
sf::Sprite					SFML_Displayer::get_food() const{
	return _food;
}
sf::Sprite					SFML_Displayer::get_walls() const{
	return _walls;
}
sf::Sprite					SFML_Displayer::get_background() const{
	return _background;
}
sf::Sprite					SFML_Displayer::get_menu() const{
	return _menu;
}




std::string SFML_Displayer::getResourcePath(const std::string & asset){
	//This will hold the base resource path: home/path/to/project
	//We give it static lifetime so that we'll only need to call
	//getwd() once to get the executable path
	char buf[MAXPATHLEN];
	static std::string baseRes;
	if (baseRes.empty()){
		//getcwd() will return NULL if something went wrong in retrieving the path
		if ((getwd(buf))){

			baseRes.append(buf);
			baseRes.append("/assets/");
		}
		else {
			perror("getResourcePath():");
			throw Except_SFML_Displayer(12, "getwd() failed");
		}
	}
	return baseRes + asset;
}

void SFML_Displayer::create_message_window_error_size(sf::VideoMode DM){
	_window_size_error.append("SDL library : A map of ");
	_window_size_error.append(std::to_string(_height));
	_window_size_error.append(" x ");
	_window_size_error.append(std::to_string(_width));
	_window_size_error.append(" cannot be create, your screen is too small (");
	_window_size_error.append(std::to_string(DM.height));
	_window_size_error.append(" x ");
	_window_size_error.append(std::to_string(DM.width));
	_window_size_error.append("). Please change your map size.");

}

bool SFML_Displayer::window_size_ok(int height, int width){
	sf::VideoMode DM;

	DM = sf::VideoMode::getDesktopMode();

	if (static_cast<unsigned int>(((height + 2) * TILE_SIZE)) > DM.height){
		create_message_window_error_size(DM);
		return false;
	}
	if (static_cast<unsigned int>((width * TILE_SIZE)) > DM.width){
		create_message_window_error_size(DM);
		return false;
	}
	return true;
}


void SFML_Displayer::init(int height, int width){

	_width = (width + 2 ) * TILE_SIZE;
	_height = (height + 2) * TILE_SIZE;

	 if (!window_size_ok(height + 2, width + 2)){
		std::cerr << _window_size_error << std::endl;
		throw Except_SFML_Displayer(0, _window_size_error);
	}

	_window = new sf::RenderWindow(sf::VideoMode(_width, _height + (2* TILE_SIZE)), "Nibbler SFML", sf::Style::Close);

	if (!_window){
		throw Except_SFML_Displayer(14, "RenderWindow() failed");
	}

	load_food();
	load_walls();
	load_background();
	load_snake_heads();
	load_snake_tails();
	load_snake_body();
	load_numbers();
	load_score();
	clear_window();
	_menu_load = false;
}

void SFML_Displayer::clear_window(){
	_window->clear(sf::Color::Black);
	draw_background();
}

void SFML_Displayer::display_all(){
	_window->display();
}



//menu handling

void SFML_Displayer::load_menu(){
	sf::Texture image;
	_images[36] = image;
	if(_images[36].loadFromFile(getResourcePath("menu_all.bmp"))) {
		_menu.setTexture(_images[36]);
	}else{
		throw Except_SFML_Displayer(17, "load_menu() failed");
	}

	if (!_menu.getTexture()){
		throw Except_SFML_Displayer(17, "load_menu() failed");
	}
	_menu_load = true;
}

void SFML_Displayer::display_banner(){

	int  nbr;

	//  banner
	if((nbr = _width / (5 * TILE_SIZE)) > 1){
		int x = ((_width % (5 * TILE_SIZE)) / 2);
		for(int z = 0; z < nbr; z++){

			_snake[BODY_HORIZONTAL].setPosition(x , 0);
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[BODY_HORIZONTAL].setPosition(x + TILE_SIZE , 0);
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[BODY_HORIZONTAL].setPosition(x + (2 * TILE_SIZE), 0);
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[HEAD_RIGHT].setPosition(x + (3 * TILE_SIZE), 0);
			_window->draw(_snake[HEAD_RIGHT]);
			_snake[ANGLE_RIGHT_DOWN].setPosition(x + (4 * TILE_SIZE), 0);
			_window->draw(_snake[ANGLE_RIGHT_DOWN]);
			_snake[TAIL_DOWN].setPosition(x , TILE_SIZE);
			_window->draw(_snake[TAIL_DOWN]);
			_food.setPosition(x + (2 * TILE_SIZE), TILE_SIZE);
			_window->draw(_food);
			_snake[BODY_VERTICAL].setPosition(x + (4 * TILE_SIZE), TILE_SIZE);
			_window->draw(_snake[BODY_VERTICAL]);
			_snake[ANGLE_RIGHT_UP].setPosition(x , (2 * TILE_SIZE));
			_window->draw(_snake[ANGLE_RIGHT_UP]);
			_snake[BODY_HORIZONTAL].setPosition(x + TILE_SIZE, (2 * TILE_SIZE));
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[BODY_HORIZONTAL].setPosition(x + (2 * TILE_SIZE), (2 * TILE_SIZE));
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[BODY_HORIZONTAL].setPosition(x + (3 * TILE_SIZE), (2 * TILE_SIZE));
			_window->draw(_snake[BODY_HORIZONTAL]);
			_snake[ANGLE_LEFT_UP].setPosition(x + (4 * TILE_SIZE), (2 * TILE_SIZE));
			_window->draw(_snake[ANGLE_LEFT_UP]);
			x = x + (5 * TILE_SIZE);
		}
	}
}

int SFML_Displayer::display_menu(std::string *menu, int size){

	if (_menu_load == false)
	{
		load_menu();
	}
	display_banner();

	_menu.setPosition((_width - 320) /2 , (_height -136) / 2);
	_window->draw(_menu);
	_window->setKeyRepeatEnabled(false);
	display_all();

	while(1)
	{
		switch (get_input()){
			case key_quit:
				return -1;
				break;
			case key_one:
				return 0;
				break;
			case key_two:
				return 1;
				break;
			default:
				break;
		}
	}
	return -1;
	static_cast<void>(size);
	static_cast<void>(menu);
}

void SFML_Displayer::close(){
	_window->close();
}

// background handling
void SFML_Displayer::load_background(){
	sf::Texture image;
	_images[2] = image;
	if(_images[2].loadFromFile(getResourcePath("background.bmp"))) {
		_background.setTexture(_images[2]);
	}else{
		throw Except_SFML_Displayer(17, "load_background() failed");
	}

	if (!_background.getTexture()){
		throw Except_SFML_Displayer(17, "load_background() failed");
	}
}

void SFML_Displayer::draw_background(){

	//Determine how many tiles we'll need to fill the screen
	int xTiles = _width / TILE_SIZE;
	int yTiles = _height / TILE_SIZE;

	//Draw the tiles by calculating their positions
	for (int i = 0; i < xTiles * yTiles; ++i){
		int x = i % xTiles;
		int y = i / xTiles;
		_background.setPosition(x * TILE_SIZE, y * TILE_SIZE);
		_window->draw(_background);
	}
}

void SFML_Displayer::draw_one_background_tile(int x, int y){
	_background.setPosition(x * TILE_SIZE, y * TILE_SIZE);
	_window->draw(_background);
}


eKey SFML_Displayer::get_input() const {
	sf::Event event;
	eKey key_pressed = key_undef;
	_window->setKeyRepeatEnabled(false);


	while (_window->pollEvent(event))
	{
			switch (event.type){
				case sf::Event::Closed:
					return key_quit;
				case sf::Event::KeyPressed:
				{
					try {
						 key_pressed = key_tab.at(event.key.code);
					}catch (std::out_of_range const & e)
					{
						key_pressed = key_undef;
					}
					break;
				}
				default :
					break;
			}
	}
	return (key_pressed);
}


// score and clock handling
void SFML_Displayer::load_numbers(){
	sf::Texture img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12, img13;
	_images[17] = img1;
	_images[18] = img2;
	_images[19] = img3;
	_images[20] = img4;
	_images[21] = img5;
	_images[22] = img6;
	_images[23] = img7;
	_images[24] = img8;
	_images[25] = img9;
	_images[26] = img10;
	_images[27] = img11;
	_images[28] = img12;
	_images[29] = img13;
	if(    _images[17].loadFromFile(getResourcePath("number_0.bmp"))
		&& _images[18].loadFromFile(getResourcePath("number_1.bmp"))
		&& _images[19].loadFromFile(getResourcePath("number_2.bmp"))
		&& _images[20].loadFromFile(getResourcePath("number_3.bmp"))
		&& _images[21].loadFromFile(getResourcePath("number_4.bmp"))
		&& _images[22].loadFromFile(getResourcePath("number_5.bmp"))
		&& _images[23].loadFromFile(getResourcePath("number_6.bmp"))
		&& _images[24].loadFromFile(getResourcePath("number_7.bmp"))
		&& _images[25].loadFromFile(getResourcePath("number_8.bmp"))
		&& _images[26].loadFromFile(getResourcePath("number_9.bmp"))
		&& _images[27].loadFromFile(getResourcePath("colon.bmp"))
		&& _images[28].loadFromFile(getResourcePath("letter_s.bmp"))
		&& _images[29].loadFromFile(getResourcePath("letter_m.bmp"))
		){

		_numbers[0].setTexture(_images[17]);
		_numbers[1].setTexture(_images[18]);
		_numbers[2].setTexture(_images[19]);
		_numbers[3].setTexture(_images[20]);
		_numbers[4].setTexture(_images[21]);
		_numbers[5].setTexture(_images[22]);
		_numbers[6].setTexture(_images[23]);
		_numbers[7].setTexture(_images[24]);
		_numbers[8].setTexture(_images[25]);
		_numbers[9].setTexture(_images[26]);
		_numbers[10].setTexture(_images[27]);
		_numbers[11].setTexture(_images[28]);
		_numbers[12].setTexture(_images[29]);

	}else{
		throw Except_SFML_Displayer(17, "load_numbers() failed");
	}

	if (	!_numbers[0].getTexture()
		&& !_numbers[1].getTexture()
		&& !_numbers[2].getTexture()
		&& !_numbers[3].getTexture()
		&& !_numbers[4].getTexture()
		&& !_numbers[5].getTexture()
		&& !_numbers[6].getTexture()
		&& !_numbers[7].getTexture()
		&& !_numbers[8].getTexture()
		&& !_numbers[9].getTexture()
		&& !_numbers[10].getTexture()
		&& !_numbers[11].getTexture()
		&& !_numbers[12].getTexture()
		){
		throw Except_SFML_Displayer(17, "load_numbers() failed");
	}
}

void SFML_Displayer::load_score(){
	sf::Texture img1, img2, img3, img4, img5, img6;
	_images[30] = img1;
	_images[31] = img2;
	_images[32] = img3;
	_images[33] = img4;
	_images[34] = img5;
	_images[35] = img6;
	if(    _images[30].loadFromFile(getResourcePath("colon.bmp"))
		&& _images[31].loadFromFile(getResourcePath("letter_s.bmp"))
		&& _images[32].loadFromFile(getResourcePath("letter_c.bmp"))
		&& _images[33].loadFromFile(getResourcePath("letter_o.bmp"))
		&& _images[34].loadFromFile(getResourcePath("letter_r.bmp"))
		&& _images[35].loadFromFile(getResourcePath("letter_e.bmp"))
		){

		_score[0].setTexture(_images[30]);
		_score[1].setTexture(_images[31]);
		_score[2].setTexture(_images[32]);
		_score[3].setTexture(_images[33]);
		_score[4].setTexture(_images[34]);
		_score[5].setTexture(_images[35]);

	}else{
		throw Except_SFML_Displayer(17, "load_score() failed");
	}

	if (	!_score[0].getTexture()
		&& !_score[1].getTexture()
		&& !_score[2].getTexture()
		&& !_score[3].getTexture()
		&& !_score[4].getTexture()
		&& !_score[5].getTexture()
		){
		throw Except_SFML_Displayer(17, "load_score() failed");
	}

}

void SFML_Displayer::draw_str_score(){
	_score[1].setPosition(_width - (6 * TILE_SIZE), _height);
	_window->draw(_score[1]);
	_score[2].setPosition(_width - (5 * TILE_SIZE), _height);
	_window->draw(_score[2]);
	_score[3].setPosition(_width - (4 * TILE_SIZE), _height);
	_window->draw(_score[3]);
	_score[4].setPosition(_width - (3 * TILE_SIZE), _height);
	_window->draw(_score[4]);
	_score[5].setPosition(_width - (2 * TILE_SIZE), _height);
	_window->draw(_score[5]);
	_score[0].setPosition(_width - (1 * TILE_SIZE), _height);
	_window->draw(_score[0]);
}

void SFML_Displayer::insert_score(std::string score){
	if (score.size() > 0 && score.size() * TILE_SIZE <= _width /2)
	{
		draw_str_score();

		int x = _width - (score.size() * TILE_SIZE);
		int y = _height + TILE_SIZE;
		for (unsigned i=0; i < score.length(); ++i)
		{

			switch(score.at(i)){
				case '0':
					_numbers[0].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[0]);
					break;
				case '1':
					_numbers[1].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[1]);
					break;
				case '2':
					_numbers[2].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[2]);
					break;
				case '3':
					_numbers[3].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[3]);
					break;
				case '4':
					_numbers[4].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[4]);
					break;
				case '5':
					_numbers[5].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[5]);
					break;
				case '6':
					_numbers[6].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[6]);
					break;
				case '7':
					_numbers[7].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[7]);
					break;
				case '8':
					_numbers[8].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[8]);
					break;
				case '9':
					_numbers[9].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[9]);
					break;
				case ':':
					_numbers[10].setPosition(x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[10]);
					break;
			}
		}
	}else
		throw Except_SFML_Displayer(1, "SDL library");

}

void SFML_Displayer::insert_clock(std::string clock){
	if (clock.size() > 0 && clock.size() < 7){
		int x = 0;
		int y = _height + TILE_SIZE;
		for (unsigned i=0; i < clock.length(); ++i)
		{
			switch(clock.at(i)){
				case '0':
					_numbers[0].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[0]);
					break;
				case '1':
					_numbers[1].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[1]);
					break;
				case '2':
					_numbers[2].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[2]);
					break;
				case '3':
					_numbers[3].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[3]);
					break;
				case '4':
					_numbers[4].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[4]);
					break;
				case '5':
					_numbers[5].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[5]);
					break;
				case '6':
					_numbers[6].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[6]);
					break;
				case '7':
					_numbers[7].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[7]);
					break;
				case '8':
					_numbers[8].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[8]);
					break;
				case '9':
					_numbers[9].setPosition( x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[9]);
					break;
				case ':':
					_numbers[10].setPosition(x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[10]);
					break;
				case 's':
					_numbers[11].setPosition(x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[11]);
					break;
				case 'm':
					_numbers[12].setPosition(x + (i * TILE_SIZE) , y);
					_window->draw(_numbers[12]);
					break;
			}
		}
	}else
		throw Except_SFML_Displayer(2, "SDL library");

}


// Snake handling
void SFML_Displayer::load_snake_heads(){
	sf::Texture img1, img2, img3, img4;
	_images[3] = img1;
	_images[4] = img2;
	_images[5] = img3;
	_images[6] = img4;

	if(    _images[3].loadFromFile(getResourcePath("snake_head_up.bmp"))
		&& _images[4].loadFromFile(getResourcePath("snake_head_right.bmp"))
		&& _images[5].loadFromFile(getResourcePath("snake_head_down.bmp"))
		&& _images[6].loadFromFile(getResourcePath("snake_head_left.bmp"))
	){

		_snake[0].setTexture(_images[3]);
		_snake[1].setTexture(_images[4]);
		_snake[2].setTexture(_images[5]);
		_snake[3].setTexture(_images[6]);
	}else{
		throw Except_SFML_Displayer(17, "load_snake_heads() failed");
	}

	if (!_snake[0].getTexture()
		&& !_snake[1].getTexture()
		&& !_snake[2].getTexture()
		&& !_snake[3].getTexture()
		){
		throw Except_SFML_Displayer(17, "load_snake_heads() failed");
	}
}

void SFML_Displayer::load_snake_body(){
sf::Texture img1, img2, img3, img4, img5, img6;
	_images[7] = img1;
	_images[8] = img2;
	_images[9] = img3;
	_images[10] = img4;
	_images[11] = img5;
	_images[12] = img6;

	if(    _images[7].loadFromFile(getResourcePath("snake_turn_right_up.bmp"))
		&& _images[8].loadFromFile(getResourcePath("snake_turn_right_down.bmp"))
		&& _images[9].loadFromFile(getResourcePath("snake_turn_left_up.bmp"))
		&& _images[10].loadFromFile(getResourcePath("snake_turn_left_down.bmp"))
		&& _images[11].loadFromFile(getResourcePath("snake_vertical.bmp"))
		&& _images[12].loadFromFile(getResourcePath("snake_horizontal.bmp"))
		){

		_snake[4].setTexture(_images[7]);
		_snake[5].setTexture(_images[8]);
		_snake[6].setTexture(_images[9]);
		_snake[7].setTexture(_images[10]);
		_snake[8].setTexture(_images[11]);
		_snake[9].setTexture(_images[12]);
	}else{
		throw Except_SFML_Displayer(17, "load_snake_body() failed");
	}

	if (	!_snake[4].getTexture()
		&& !_snake[5].getTexture()
		&& !_snake[6].getTexture()
		&& !_snake[7].getTexture()
		&& !_snake[8].getTexture()
		&& !_snake[9].getTexture()
		){
		throw Except_SFML_Displayer(17, "load_snake_body() failed");
	}
}

void SFML_Displayer::load_snake_tails(){

	sf::Texture img1, img2, img3, img4;
	_images[13] = img1;
	_images[14] = img2;
	_images[15] = img3;
	_images[16] = img4;

	if(    _images[13].loadFromFile(getResourcePath("snake_tail_up.bmp"))
		&& _images[14].loadFromFile(getResourcePath("snake_tail_right.bmp"))
		&& _images[15].loadFromFile(getResourcePath("snake_tail_down.bmp"))
		&& _images[16].loadFromFile(getResourcePath("snake_tail_left.bmp"))
	){

		_snake[10].setTexture(_images[13]);
		_snake[11].setTexture(_images[14]);
		_snake[12].setTexture(_images[15]);
		_snake[13].setTexture(_images[16]);
	}else{
		throw Except_SFML_Displayer(17, "load_snake_tails() failed");
	}

	if (!_snake[10].getTexture()
		&& !_snake[11].getTexture()
		&& !_snake[12].getTexture()
		&& !_snake[13].getTexture()
		){
		throw Except_SFML_Displayer(17, "load_snake_tails() failed");
	}
}

void SFML_Displayer::insert_snake(std::list<SnakeObject> snake){
	if (snake.size() == 0)
		throw Except_SFML_Displayer(5, "SDL library");
	//draw_all_snake
	for (std::list<SnakeObject>::iterator it = snake.begin();it != snake.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE)  >= _width -(2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE) >= _height -(2 * TILE_SIZE ))
			throw Except_SFML_Displayer(3, "SDL library");
		_snake[it->getAsset()].setPosition((it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
		_window->draw(_snake[it->getAsset()]);
	}
}

// Food handling
void SFML_Displayer::delete_food(std::list<Point> food){
	for (std::list<Point>::iterator it = food.begin();it != food.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE)  >= _width -(2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE)  >= _height -(2 * TILE_SIZE ))
			throw Except_SFML_Displayer(6, "SDL library");
		draw_one_background_tile((it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE) + TILE_SIZE);
	}
}

void SFML_Displayer::load_food(){

	sf::Texture image;
	_images[0] = image;
	if(_images[0].loadFromFile(getResourcePath("food.bmp"))) {
		_food.setTexture(_images[0]);
	}else{
		throw Except_SFML_Displayer(17, "load_food() failed");
	}

	if (!_food.getTexture()){
		throw Except_SFML_Displayer(17, "load_food() failed");
	}

}

void SFML_Displayer::insert_food(std::list<Point> food) {

	if (food.empty())
		return;
	if (_old_food.size() != 0)
		delete_food(_old_food);
	 _old_food = food;
	for (std::list<Point>::iterator it = food.begin(); it != food.end(); ++it){
		if (static_cast<size_t>(it->getX())  * TILE_SIZE  >= _width - (2 * TILE_SIZE )|| static_cast<size_t>(it->getY() * TILE_SIZE)  >= _height - (2 * TILE_SIZE ))
			throw Except_SFML_Displayer(7, "SDL library");
		_food.setPosition((it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
		_window->draw(_food);
	 }

}


// Walls handling

void SFML_Displayer::load_walls(){
	sf::Texture image;
	_images[1] = image;
	if(_images[1].loadFromFile(getResourcePath("wall2.bmp"))) {
		_walls.setTexture(_images[1]);
	}else{
		throw Except_SFML_Displayer(17, "load_walls() failed");
	}
	if (!_walls.getTexture()){
		throw Except_SFML_Displayer(17, "load_walls() failed");
	}
}

void SFML_Displayer::draw_walls_around_map(){
	// draw horizontal walls
	for (size_t i = 0;  i  * TILE_SIZE <= _width ; i++){
		_walls.setPosition(i * TILE_SIZE, 0);
		_window->draw(_walls);
		_walls.setPosition(i * TILE_SIZE, _height - TILE_SIZE);
		_window->draw(_walls);
	}

	// draw vertical walls
	for (size_t i = 0;  i  * TILE_SIZE <= _height - TILE_SIZE ; i++){
		_walls.setPosition(0, i * TILE_SIZE);
		_window->draw(_walls);
		_walls.setPosition(_width - TILE_SIZE, i * TILE_SIZE);
		_window->draw(_walls);
	}
}

void SFML_Displayer::insert_walls(std::list<Point> walls){
	draw_walls_around_map();
	if (walls.empty())
		return;
	for (std::list<Point>::iterator it = walls.begin(); it != walls.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE) >= _width - (2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE) >= _height -(2 * TILE_SIZE ))
			throw Except_SFML_Displayer(10, "SDL library");
		_walls.setPosition((it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
		_window->draw(_walls);
	}
}

/*
** Exception class
*/

SFML_Displayer::Except_SFML_Displayer::Except_SFML_Displayer(unsigned int n, std::string msg) : _n(n){
	_errors[0] = static_cast<std::string>("");
	_errors[1] = static_cast<std::string>(" : score string is empty or is too long to feet the window.");
	_errors[2] = static_cast<std::string>(" : clock string is empty or too long.");
	_errors[3] = static_cast<std::string>(" : snake coordinates are out of the map.");
	_errors[4] = static_cast<std::string>(" : _old_snake coordinates are out of the map.");
	_errors[5] = static_cast<std::string>(" : snake coordinates are empty.");
	_errors[6] = static_cast<std::string>(" : _old_food coordinates are out of the map.");
	_errors[7] = static_cast<std::string>(" : food coordinates are out of the map.");
	_errors[8] = static_cast<std::string>(" : food coordinates are empty.");
	_errors[9] = static_cast<std::string>(" : walls coordinates are empty.");
	_errors[10] = static_cast<std::string>(" : walls coordinates are out of the map.");
	_errors[11] = static_cast<std::string>("SFML library: CreateTextureFromSurface error: ");
	_errors[12] = static_cast<std::string>("SFML library: LoadBMP error: ");
	_errors[13] = static_cast<std::string>("SFML library: SFML_Init error: ");
	_errors[14] = static_cast<std::string>("SFML library: CreateWindow error: ");
	_errors[15] = static_cast<std::string>("SFML library: CreateRenderer error: ");
	_errors[16] = static_cast<std::string>("SFML library: GetCurrentDisplayMode error: ");
	_errors[17] = static_cast<std::string>("SFML library: loadFromFile error: ");


	if (_n < 11)
		_errors[n].insert(0, msg);
	else
		_errors[n].append(msg);


}

SFML_Displayer::Except_SFML_Displayer::Except_SFML_Displayer(const Except_SFML_Displayer & src){
	static_cast<void>(src);
}

SFML_Displayer::Except_SFML_Displayer::~Except_SFML_Displayer() throw(){
}

SFML_Displayer::Except_SFML_Displayer & SFML_Displayer::Except_SFML_Displayer::operator=(const Except_SFML_Displayer & rhs){

	static_cast<void>(rhs);
	return *this;
}

const char *SFML_Displayer::Except_SFML_Displayer::what() const throw(){
	return (_errors[_n].c_str());
}

std::ostream &			operator<<(std::ostream & o, SFML_Displayer::Except_SFML_Displayer const & i){
	o << i.what();
	return o;
}

