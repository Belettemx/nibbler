#include "SDL_Displayer.hpp"


SDL_Displayer::SDL_Displayer(): _win(nullptr), _food(nullptr), _walls(nullptr), _background(nullptr){
	key_tab[SDLK_LEFT] = key_left;
	key_tab[SDLK_RIGHT] = key_right;
	key_tab[SDLK_UP] = key_up;
	key_tab[SDLK_DOWN] = key_down;
	key_tab[SDLK_a] = key_a;
	key_tab[SDLK_d] = key_d;
	key_tab[SDLK_q] = key_quit;
	key_tab[SDL_QUIT] = key_quit;
	key_tab[SDLK_ESCAPE] = key_quit; // escape key
	key_tab[SDLK_KP_ENTER] = key_enter;
	key_tab[SDLK_1] = key_one;
	key_tab[SDLK_2] = key_two;
	key_tab[SDLK_3] = key_three;

}

SDL_Displayer::~SDL_Displayer(){
	cleanup(_background, _food, _renderer, _win, _walls);
	for (int i =0; i < 15; i++){
		if (_snake[i]){
			SDL_DestroyTexture(_snake[i]);
		}
	}
}

SDL_Displayer::SDL_Displayer(const SDL_Displayer & src){
	_win = src.get_win();
	_renderer = src.get_renderer();
	_menu = src.get_menu();
	key_tab = src.get_key_tab();
	_height = src.get_height();
	_width = src.get_width();
	_old_snake = src.get_old_snake();
	_old_food = src.get_old_food();
	_window_size_error = src.get_window_size_error();
	load_food();
	load_walls();
	load_background();
	load_snake_heads();
	load_snake_tails();
	load_snake_body();
	load_score();
	load_numbers();

}

SDL_Displayer & SDL_Displayer::operator=(const SDL_Displayer & src){
	_win = src.get_win();
	_renderer = src.get_renderer();
	_menu = src.get_menu();
	key_tab = src.get_key_tab();
	_height = src.get_height();
	_width = src.get_width();
	_old_snake = src.get_old_snake();
	_old_food = src.get_old_food();
	_window_size_error = src.get_window_size_error();
	return *this;
}


SDL_Window				*SDL_Displayer::get_win() const {
	return _win;
}

SDL_Renderer			*SDL_Displayer::get_renderer() const {
	return _renderer;
}

SDL_Texture				*SDL_Displayer::get_menu() const {
	return _menu;
}

std::map <int, eKey>	SDL_Displayer::get_key_tab() const {
	return key_tab;
}

size_t					SDL_Displayer::get_height() const {
	return _height;
}

size_t					SDL_Displayer::get_width() const {
	return _width;
}

std::list<Point>		SDL_Displayer::get_old_snake() const {
	return _old_snake;
}

std::list<Point>		SDL_Displayer::get_old_food() const {
	return _old_food;
}

std::string				SDL_Displayer::get_window_size_error() const {
	return _window_size_error;
}

/**
* Loads a BMP image into a texture on the rendering device
* @param file The BMP image file to load
* @param ren The renderer to load the texture onto
* @return the loaded texture, or nullptr if something went wrong.
*/
SDL_Texture* SDL_Displayer::loadTexture(const std::string &file, SDL_Renderer *ren){

	//Initialize to nullptr to avoid dangling pointer issues
	SDL_Texture *texture = nullptr;
	//Load the image
	SDL_Surface *loadedImage = SDL_LoadBMP(file.c_str());
	//If the loading went ok, convert to texture and return the texture
	if (loadedImage != nullptr){
		//SDL_BlitSurface(loadedImage, &spriteSrc, screen, &spriteCoord);
		texture = SDL_CreateTextureFromSurface(ren, loadedImage);
		SDL_FreeSurface(loadedImage);
		//Make sure converting went ok too
		if (texture == nullptr){
			throw Except_SDL_Displayer(11, SDL_GetError());
		}
	}
	else {
		throw Except_SDL_Displayer(12, file + " " + SDL_GetError());
	}
	return texture;
}

/**
* Draw an SDL_Texture to an SDL_Renderer at position x, y, preserving
* the texture's width and height
* @param tex The source texture we want to draw
* @param ren The renderer we want to draw to
* @param x The x coordinate to draw to
* @param y The y coordinate to draw to
*/
void SDL_Displayer::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;

	//SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	dst.h = TEXTURE_HEIGHT;
	dst.w = TEXTURE_WIDTH;
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

void SDL_Displayer::renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, int texture_width, int texture_height){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;

	//SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	dst.h = texture_height;
	dst.w = texture_width;
	SDL_RenderCopy(ren, tex, NULL, &dst);
}

std::string SDL_Displayer::getResourcePath(const std::string & asset){
	//This will hold the base resource path: home/path/to/project
	//We give it static lifetime so that we'll only need to call
	//SDL_GetBasePath once to get the executable path
	static std::string baseRes;

	if (baseRes.empty()){
		//SDL_GetBasePath will return NULL if something went wrong in retrieving the path
		char *basePath = SDL_GetBasePath();
		if (basePath){
			baseRes = basePath;
			baseRes.append("assets/");
			SDL_free(basePath);
		}
		else {
			throw Except_SDL_Displayer(12, SDL_GetError());
		}
	}
	return baseRes + asset;
}

void SDL_Displayer::create_message_window_error_size(SDL_DisplayMode DM){
	_window_size_error.append("SDL library : A map of ");
	_window_size_error.append(std::to_string(_height));
	_window_size_error.append(" x ");
	_window_size_error.append(std::to_string(_width));
	_window_size_error.append(" cannot be create, your screen is too small (");
	_window_size_error.append(std::to_string(DM.h));
	_window_size_error.append(" x ");
	_window_size_error.append(std::to_string(DM.w));
	_window_size_error.append("). Please change your map size.");

}

bool SDL_Displayer::window_size_ok(int height, int width){

	SDL_DisplayMode DM;
	if (SDL_GetCurrentDisplayMode(0, &DM) != 0){
		throw Except_SDL_Displayer(16, SDL_GetError());
	}

	if (((height + 2) * TILE_SIZE) > DM.h){
		create_message_window_error_size(DM);
		return false;
	}
	if ((width * TILE_SIZE) > DM.w){
		create_message_window_error_size(DM);
		return false;
	}
	return true;
}


void SDL_Displayer::init(int height, int width){

	if (SDL_Init(SDL_INIT_EVERYTHING | SDL_INIT_EVENTS) != 0){
		throw Except_SDL_Displayer(13, SDL_GetError());
	}

	// save windows size with the extra lenght needed to draw the walls
	_width = (width + 2) * TILE_SIZE;
	_height = (height + 2) * TILE_SIZE;

	if (!window_size_ok(height + 2, width + 2)){
		throw Except_SDL_Displayer(0, _window_size_error);
	}


	_win = SDL_CreateWindow("Nibbler SDL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _width, _height + (2* TILE_SIZE), SDL_WINDOW_SHOWN);
	if (_win == nullptr){
		SDL_Quit();
		throw Except_SDL_Displayer(14, SDL_GetError());
	}

	_renderer = SDL_CreateRenderer(_win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (_renderer == nullptr){
		cleanup(_win);
		SDL_Quit();
		throw Except_SDL_Displayer(15, SDL_GetError());
	}
	load_food();
	load_walls();
	load_background();
	load_snake_heads();
	load_snake_tails();
	load_snake_body();
	load_score();
	load_numbers();
	clear_window();
	_menu_load = false;
}

void SDL_Displayer::clear_window(){
	SDL_RenderClear(_renderer);
	draw_background();
}

void SDL_Displayer::display_all(){
	SDL_RenderPresent(_renderer);
}



//menu handling

void SDL_Displayer::load_menu(){

	_menu = loadTexture(getResourcePath("menu_all.bmp"), _renderer);

	if (_menu == nullptr ){
			throw Except_SDL_Displayer(17, "load_menu() failed");
	}
	_menu_load = true;
}

void SDL_Displayer::display_banner(){

	int  nbr;

	//  banner
	if((nbr = _width / (5 * TILE_SIZE)) > 1){
		int x = ((_width % (5 * TILE_SIZE)) / 2);
		for(int z = 0; z < nbr; z++){
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x , 0);
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x + TILE_SIZE , 0);
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x + (2 * TILE_SIZE), 0);
			renderTexture(_snake[HEAD_RIGHT], _renderer, x + (3 * TILE_SIZE), 0);
			renderTexture(_snake[ANGLE_RIGHT_DOWN], _renderer, x + (4 * TILE_SIZE), 0);
			renderTexture(_snake[TAIL_DOWN], _renderer, x , TILE_SIZE);
			renderTexture(_food, _renderer, x + (2 * TILE_SIZE), TILE_SIZE);
			renderTexture(_snake[BODY_VERTICAL], _renderer, x + (4 * TILE_SIZE), TILE_SIZE);
			renderTexture(_snake[ANGLE_RIGHT_UP], _renderer, x , (2 * TILE_SIZE));
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x + TILE_SIZE, (2 * TILE_SIZE));
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x + (2 * TILE_SIZE), (2 * TILE_SIZE));
			renderTexture(_snake[BODY_HORIZONTAL], _renderer, x + (3 * TILE_SIZE), (2 * TILE_SIZE));
			renderTexture(_snake[ANGLE_LEFT_UP], _renderer, x + (4 * TILE_SIZE), (2 * TILE_SIZE));
			x = x + (5 * TILE_SIZE);
		}
	}
}

int SDL_Displayer::display_menu(std::string *menu, int size){
	static_cast<void>(size);
	static_cast<void>(menu);
	if (_menu_load == false)
	{
		load_menu();
	}
	display_banner();

	renderTexture(_menu, _renderer, (_width - 320) /2 , (_height -136) / 2, 320, 136);
	display_all();

	SDL_Event event;
	while (1){
		while(SDL_PollEvent(&event)){
			switch( event.type ){
				/* Look for a keypress */
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym){
						case SDLK_1:
							return 0;
							break;
						case SDLK_2:
							return 1;
							break;
						case SDLK_q:
							return -1;
							break;
						case SDLK_ESCAPE:
							return -1;
							break;
					}
					break;
				case SDL_QUIT:
					return  -1;
					break;
			}
		}
	}
	return -1;
}

void SDL_Displayer::close(){
	SDL_Quit();
}

// background handling
void SDL_Displayer::load_background(){
	_background = loadTexture(getResourcePath("background.bmp"), _renderer);
	if (_background == nullptr){
			throw Except_SDL_Displayer(17, "load_background() failed");
	}
}

void SDL_Displayer::draw_background(){

	//Determine how many tiles we'll need to fill the screen
	int xTiles = _width / TILE_SIZE;
	int yTiles = _height / TILE_SIZE;

	//Draw the tiles by calculating their positions
	for (int i = 0; i < xTiles * yTiles; ++i){
		int x = i % xTiles;
		int y = i / xTiles;
		renderTexture(_background, _renderer, x * TILE_SIZE, y * TILE_SIZE);
	}
}

void SDL_Displayer::draw_one_background_tile(int x, int y){
	renderTexture(_background, _renderer, x * TILE_SIZE, y * TILE_SIZE);
}


eKey SDL_Displayer::get_input() const {
	SDL_Event event;
	SDL_PumpEvents();
	// SDL_FlushEvents(SDL_USEREVENT, SDL_LASTEVENT);
	SDL_PollEvent(&event);
	eKey key_pressed = key_undef;

	try {
		switch( event.type ){
			/* Look for a keypress */
			case SDL_KEYDOWN:
				key_pressed = key_tab.at(event.key.keysym.sym);
				break;
			case SDL_QUIT:
				key_pressed = key_quit;
				break;
		}
	}catch(std::out_of_range const & e)
	{
		key_pressed = key_undef;
	}
	SDL_PumpEvents();
	while(SDL_PollEvent(&event));
	// SDL_FlushEvents(SDL_USEREVENT, SDL_LASTEVENT);
	return (key_pressed);
}


// score and clock handling
void SDL_Displayer::load_numbers(){
	_numbers[0] = loadTexture(getResourcePath("number_0.bmp"), _renderer);
	_numbers[1] = loadTexture(getResourcePath("number_1.bmp"), _renderer);
	_numbers[2] = loadTexture(getResourcePath("number_2.bmp"), _renderer);
	_numbers[3] = loadTexture(getResourcePath("number_3.bmp"), _renderer);
	_numbers[4] = loadTexture(getResourcePath("number_4.bmp"), _renderer);
	_numbers[5] = loadTexture(getResourcePath("number_5.bmp"), _renderer);
	_numbers[6] = loadTexture(getResourcePath("number_6.bmp"), _renderer);
	_numbers[7] = loadTexture(getResourcePath("number_7.bmp"), _renderer);
	_numbers[8] = loadTexture(getResourcePath("number_8.bmp"), _renderer);
	_numbers[9] = loadTexture(getResourcePath("number_9.bmp"), _renderer);
	_numbers[10] = loadTexture(getResourcePath("colon.bmp"), _renderer);
	_numbers[11] = loadTexture(getResourcePath("letter_s.bmp"), _renderer);
	_numbers[12] = loadTexture(getResourcePath("letter_m.bmp"), _renderer);
	if (_numbers[0] == nullptr
		|| _numbers[1] == nullptr
		|| _numbers[2] == nullptr
		|| _numbers[3] == nullptr
		|| _numbers[4] == nullptr
		|| _numbers[5] == nullptr
		|| _numbers[6] == nullptr
		|| _numbers[7] == nullptr
		|| _numbers[8] == nullptr
		|| _numbers[9] == nullptr
		|| _numbers[10] == nullptr
		|| _numbers[11] == nullptr
		|| _numbers[12] == nullptr
		){

			throw Except_SDL_Displayer(17, "load_numbers() failed");
	}
}

void SDL_Displayer::load_score(){
	_score[0] = loadTexture(getResourcePath("colon.bmp"), _renderer);
	_score[1] = loadTexture(getResourcePath("letter_s.bmp"), _renderer);
	_score[2] = loadTexture(getResourcePath("letter_c.bmp"), _renderer);
	_score[3] = loadTexture(getResourcePath("letter_o.bmp"), _renderer);
	_score[4] = loadTexture(getResourcePath("letter_r.bmp"), _renderer);
	_score[5] = loadTexture(getResourcePath("letter_e.bmp"), _renderer);

	if (_score[0] == nullptr
		|| _score[1] == nullptr
		|| _score[2] == nullptr
		|| _score[3] == nullptr
		|| _score[4] == nullptr
		|| _score[5] == nullptr
		){

			throw Except_SDL_Displayer(17, "load_score() failed");
	}
}

void SDL_Displayer::draw_str_score(){
	renderTexture(_score[1], _renderer, _width - (6 * TILE_SIZE), _height);
	renderTexture(_score[2], _renderer, _width - (5 * TILE_SIZE), _height);
	renderTexture(_score[3], _renderer, _width - (4 * TILE_SIZE), _height);
	renderTexture(_score[4], _renderer, _width - (3 * TILE_SIZE), _height);
	renderTexture(_score[5], _renderer, _width - (2 * TILE_SIZE), _height);
	renderTexture(_score[0], _renderer, _width - (1 * TILE_SIZE), _height);
}

void SDL_Displayer::insert_score(std::string score){
	if (score.size() > 0 && score.size() * TILE_SIZE <= _width /2)
	{
		draw_str_score();

		int x = _width - (score.size() * TILE_SIZE);
		int y = _height + TILE_SIZE;
		for (unsigned i=0; i < score.length(); ++i)
		{

			switch(score.at(i)){
				case '0':
					renderTexture(_numbers[0], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '1':
					renderTexture(_numbers[1], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '2':
					renderTexture(_numbers[2], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '3':
					renderTexture(_numbers[3], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '4':
					renderTexture(_numbers[4], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '5':
					renderTexture(_numbers[5], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '6':
					renderTexture(_numbers[6], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '7':
					renderTexture(_numbers[7], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '8':
					renderTexture(_numbers[8], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '9':
					renderTexture(_numbers[9], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case ':':
					renderTexture(_numbers[10], _renderer, x + (i * TILE_SIZE) , y);
					break;

			}
		}
	}else
		throw Except_SDL_Displayer(1, "SDL library");
}

void SDL_Displayer::insert_clock(std::string clock){
	if (clock.size() > 0 && clock.size() < 7){
		int x = 0;
		int y = _height + TILE_SIZE;
		for (unsigned i=0; i < clock.length(); ++i)
		{

			switch(clock.at(i)){
				case '0':
					renderTexture(_numbers[0], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '1':
					renderTexture(_numbers[1], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '2':
					renderTexture(_numbers[2], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '3':
					renderTexture(_numbers[3], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '4':
					renderTexture(_numbers[4], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '5':
					renderTexture(_numbers[5], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '6':
					renderTexture(_numbers[6], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '7':
					renderTexture(_numbers[7], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '8':
					renderTexture(_numbers[8], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case '9':
					renderTexture(_numbers[9], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case ':':
					renderTexture(_numbers[10], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case 's':
					renderTexture(_numbers[11], _renderer, x + (i * TILE_SIZE) , y);
					break;
				case 'm':
					renderTexture(_numbers[12], _renderer, x + (i * TILE_SIZE) , y);
					break;
			}
		}
	}else
		throw Except_SDL_Displayer(2, "SDL library");
}


// Snake handling
void SDL_Displayer::load_snake_heads(){
	_snake[0] = loadTexture(getResourcePath("snake_head_up.bmp"), _renderer);
	_snake[1] = loadTexture(getResourcePath("snake_head_right.bmp"), _renderer);
	_snake[2] = loadTexture(getResourcePath("snake_head_down.bmp"), _renderer);
	_snake[3] = loadTexture(getResourcePath("snake_head_left.bmp"), _renderer);
	if (_snake[0] == nullptr || _snake[1] == nullptr || _snake[2] == nullptr || _snake[3] == nullptr){
			throw Except_SDL_Displayer(17, "load_snake_heads() failed");
	}

}

void SDL_Displayer::load_snake_body(){
	_snake[4] = loadTexture(getResourcePath("snake_turn_right_up.bmp"), _renderer);
	_snake[5] = loadTexture(getResourcePath("snake_turn_right_down.bmp"), _renderer);
	_snake[6] = loadTexture(getResourcePath("snake_turn_left_up.bmp"), _renderer);
	_snake[7] = loadTexture(getResourcePath("snake_turn_left_down.bmp"), _renderer);
	_snake[8] = loadTexture(getResourcePath("snake_vertical.bmp"), _renderer);
	_snake[9] = loadTexture(getResourcePath("snake_horizontal.bmp"), _renderer);

	if (_snake[0] == nullptr || _snake[1] == nullptr || _snake[2] == nullptr || _snake[3] == nullptr || _snake[4] == nullptr || _snake[5] == nullptr){
			throw Except_SDL_Displayer(17, "load_snake_body() failed");
	}

}

void SDL_Displayer::load_snake_tails(){
	_snake[10] = loadTexture(getResourcePath("snake_tail_up.bmp"), _renderer);
	_snake[11] = loadTexture(getResourcePath("snake_tail_right.bmp"), _renderer);
	_snake[12] = loadTexture(getResourcePath("snake_tail_down.bmp"), _renderer);
	_snake[13] = loadTexture(getResourcePath("snake_tail_left.bmp"), _renderer);
	if (_snake[0] == nullptr || _snake[1] == nullptr || _snake[2] == nullptr || _snake[3] == nullptr){
			throw Except_SDL_Displayer(17, "load_snake_tails() failed");
	}
}

void SDL_Displayer::insert_snake(std::list<SnakeObject> snake){
	if (snake.size() == 0)
		throw Except_SDL_Displayer(5, "SDL library");
	//draw_all_snake
	for (std::list<SnakeObject>::iterator it = snake.begin();it != snake.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE)  >= _width -(2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE) >= _height -(2 * TILE_SIZE ))
			throw Except_SDL_Displayer(3, "SDL library");
		renderTexture(_snake[it->getAsset()], _renderer, (it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
	}

}

// Food handling
void SDL_Displayer::delete_food(std::list<Point> food){
	for (std::list<Point>::iterator it = food.begin();it != food.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE)  >= _width -(2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE)  >= _height -(2 * TILE_SIZE ))
			throw Except_SDL_Displayer(6, "SDL library");
		draw_one_background_tile((it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE) + TILE_SIZE);
	}

}

void SDL_Displayer::load_food(){
	_food = loadTexture(getResourcePath("food.bmp"), _renderer);
	if (_food == nullptr){
		throw Except_SDL_Displayer(17, "load_food() failed");
	}
}

void SDL_Displayer::insert_food(std::list<Point> food) {

	if (food.empty())
		return;
	if (_old_food.size() != 0)
		delete_food(_old_food);
	_old_food = food;
	for (std::list<Point>::iterator it = food.begin(); it != food.end(); ++it){
		if (static_cast<size_t>(it->getX())  * TILE_SIZE  >= _width - (2 * TILE_SIZE )|| static_cast<size_t>(it->getY() * TILE_SIZE)  >= _height - (2 * TILE_SIZE ))
			throw Except_SDL_Displayer(7, "SDL library");
		renderTexture(_food, _renderer, (it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
	}

}


// Walls handling

void SDL_Displayer::load_walls(){
	_walls = loadTexture(getResourcePath("wall1.bmp"), _renderer);
	if (_walls == nullptr){
		throw Except_SDL_Displayer(17, "load_walls() failed");
	}
}

void SDL_Displayer::draw_walls_around_map(){
	// draw horizontal walls
	for (size_t i = 0;  i  * TILE_SIZE <= _width ; i++){
		renderTexture(_walls, _renderer, i * TILE_SIZE, 0);
		renderTexture(_walls, _renderer, i * TILE_SIZE, _height - TILE_SIZE);
	}

	// draw vertical walls
	for (size_t i = 0;  i  * TILE_SIZE <= _height - TILE_SIZE ; i++){
		renderTexture(_walls, _renderer, 0, i * TILE_SIZE);
		renderTexture(_walls, _renderer, _width - TILE_SIZE, i * TILE_SIZE);
	}
}

void SDL_Displayer::insert_walls(std::list<Point> walls){
	draw_walls_around_map();
	if (walls.empty())
		return;
	for (std::list<Point>::iterator it = walls.begin(); it != walls.end(); ++it){
		if (static_cast<size_t>(it->getX() * TILE_SIZE) >= _width - (2 * TILE_SIZE ) || static_cast<size_t>(it->getY() * TILE_SIZE) >= _height -(2 * TILE_SIZE ))
			throw Except_SDL_Displayer(10, "SDL library");
		renderTexture(_walls, _renderer, (it->getX() * TILE_SIZE) + TILE_SIZE, (it->getY() * TILE_SIZE ) + TILE_SIZE);
	}
}

/*
** Exception class
*/

SDL_Displayer::Except_SDL_Displayer::Except_SDL_Displayer(unsigned int n, std::string msg) : _n(n){
	_errors[0] = static_cast<std::string>("");
	_errors[1] = static_cast<std::string>(" : score string is empty or is too long to feet the window.");
	_errors[2] = static_cast<std::string>(" : clock string is empty or too long.");
	_errors[3] = static_cast<std::string>(" : snake coordinates are out of the map.");
	_errors[4] = static_cast<std::string>(" : _old_snake coordinates are out of the map.");
	_errors[5] = static_cast<std::string>(" : snake coordinates are empty.");
	_errors[6] = static_cast<std::string>(" : _old_food coordinates are out of the map.");
	_errors[7] = static_cast<std::string>(" : food coordinates are out of the map.");
	_errors[8] = static_cast<std::string>(" : food coordinates are empty.");
	_errors[9] = static_cast<std::string>(" : walls coordinates are empty.");
	_errors[10] = static_cast<std::string>(" : walls coordinates are out of the map.");
	_errors[11] = static_cast<std::string>("SDL library: CreateTextureFromSurface error: ");
	_errors[12] = static_cast<std::string>("SDL library: LoadBMP error: ");
	_errors[13] = static_cast<std::string>("SDL library: SDL_Init error: ");
	_errors[14] = static_cast<std::string>("SDL library: CreateWindow error: ");
	_errors[15] = static_cast<std::string>("SDL library: CreateRenderer error: ");
	_errors[16] = static_cast<std::string>("SDL library: GetCurrentDisplayMode error: ");
	_errors[17] = static_cast<std::string>("SDL library: loadTexture error: ");


	if (_n < 11)
		_errors[n].insert(0, msg);
	else
		_errors[n].append(msg);
}

SDL_Displayer::Except_SDL_Displayer::Except_SDL_Displayer(const Except_SDL_Displayer & src){
	static_cast<void>(src);
}

SDL_Displayer::Except_SDL_Displayer::~Except_SDL_Displayer() throw(){
}

SDL_Displayer::Except_SDL_Displayer & SDL_Displayer::Except_SDL_Displayer::operator=(const Except_SDL_Displayer & rhs){

	static_cast<void>(rhs);
	return *this;
}

const char *SDL_Displayer::Except_SDL_Displayer::what() const throw(){
	return (_errors[_n].c_str());
}

std::ostream &			operator<<(std::ostream & o, SDL_Displayer::Except_SDL_Displayer const & i){
	o << i.what();
	return o;
}

