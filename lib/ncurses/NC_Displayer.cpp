#include "NC_Displayer.hpp"


#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#define CTRLD 	4

NC_Displayer::NC_Displayer(){
	key_tab[KEY_LEFT] = key_left;
	key_tab[KEY_RIGHT] = key_right;
	key_tab[KEY_UP] = key_up;
	key_tab[KEY_DOWN] = key_down;
	key_tab['a'] = key_a;
	key_tab['d'] = key_d;
	key_tab['q'] = key_quit;
	key_tab[27] = key_quit; // escape key
	key_tab[KEY_RESIZE] = key_resize;
	key_tab[KEY_ENTER] = key_enter;
	key_tab['1'] = key_one;
	key_tab['2'] = key_two;
	key_tab['3'] = key_three;

	_snake_body[0] = '^';
	_snake_body[1] = '>';
	_snake_body[2] = 'v';
	_snake_body[3] = '<';
	_snake_body[4] = '#';
	_snake_body[5] = '#';
	_snake_body[6] = '#';
	_snake_body[7] = '#';
	_snake_body[8] = '#';
	_snake_body[9] = '#';
	_snake_body[10] = '#';
	_snake_body[11] = '#';
	_snake_body[12] = '#';
	_snake_body[13] = '#';

}

NC_Displayer::~NC_Displayer(){

}

NC_Displayer::NC_Displayer(const NC_Displayer & src){
	_win =  src.get_win();
	key_tab = src.get_key_tab();
	_height = src.get_height();
	_width = src.get_width();
	_old_food = src.get_old_food();
	_old_snake = src.get_old_snake();
	_snake_body[0] = '^';
	_snake_body[1] = '>';
	_snake_body[2] = 'v';
	_snake_body[3] = '<';
	_snake_body[4] = '#';
	_snake_body[5] = '#';
	_snake_body[6] = '#';
	_snake_body[7] = '#';
	_snake_body[8] = '#';
	_snake_body[9] = '#';
	_snake_body[10] = '#';
	_snake_body[11] = '#';
	_snake_body[12] = '#';
	_snake_body[13] = '#';
}

NC_Displayer &	NC_Displayer::operator=(const NC_Displayer & src){
	_win =  src.get_win();
	key_tab = src.get_key_tab();
	_height = src.get_height();
	_width = src.get_width();
	_old_food = src.get_old_food();
	_old_snake = src.get_old_snake();
	_snake_body[0] = '^';
	_snake_body[1] = '>';
	_snake_body[2] = 'v';
	_snake_body[3] = '<';
	_snake_body[4] = '#';
	_snake_body[5] = '#';
	_snake_body[6] = '#';
	_snake_body[7] = '#';
	_snake_body[8] = '#';
	_snake_body[9] = '#';
	_snake_body[10] = '#';
	_snake_body[11] = '#';
	_snake_body[12] = '#';
	_snake_body[13] = '#';
	return *this;
}
/*
** getter and setter
*/

WINDOW					*NC_Displayer::get_win() const{
	return this->_win;
}

std::map <int, eKey>	NC_Displayer::get_key_tab() const{
	return this->key_tab;
}

int						NC_Displayer::get_height() const{
	return this->_height;
}

int 					NC_Displayer::get_width() const{
	return this->_width;
}

std::list<Point>		NC_Displayer::get_old_food() const{
	return this->_old_food;
}


std::list<SnakeObject>	NC_Displayer::get_old_snake() const{
	return this->_old_snake;
}

bool window_size_ok(int height, int width){
	if (height > LINES || width > COLS) //COLS x 2 if we do the bonus
		return false;
	return true;
}

void NC_Displayer::init(int height, int width){

	std::string msg;
	int rows,cols;

	initscr();
	getmaxyx(stdscr,rows,cols);	/* get the number of rows and columns */
	if (!window_size_ok(height + 4, width + 2))
	{
		msg = "ncurses library : A map of ";
		msg.append(std::to_string(height));
		msg.append(" x ");
		msg.append(std::to_string(width));
		msg.append(" cannot be create, your terminal window is too small (");
		msg.append(std::to_string(rows -4));
		msg.append(" x ");
		msg.append(std::to_string(cols -2));
		msg.append("). Please resize it or change your map size.");
		refresh();
		endwin();
		throw Except_NC_Displayer(0, msg);
	}else{

		_height = static_cast<size_t>(height + 2);
		_width =  static_cast<size_t>(width + 2);
		keypad(stdscr, TRUE);
		noecho(); // disable echoing of characters on the screen
		curs_set(0); // hide the default screen cursor.
		_win = subwin(stdscr, _height, _width, 0, 0);

		nodelay(stdscr, true);
		refresh();
	}
}

void NC_Displayer::clear_window(){
	// check if still working with this function insert in the gamelogic
	clear();
	_win = subwin(stdscr, _height, _width, 0, 0);
}

void NC_Displayer::display_all(){

}


void NC_Displayer::display_banner(int size){
	std::string snake[5];
	int  nbr, pos = 0;
	// create banner
	if((nbr = _width / 19) > 1){
		try{
			for(int z = 0; z < nbr; z++){
				snake[0].append("  _,.--.   --..,_ ");
				snake[1].append("'`__ o  `;__. `' .");
				snake[2].append(":`. '---'`  `  .'.");
				snake[3].append(" '.`'--....--'`.' ");
				snake[4].append("   `'--....--'`   ");
			}
		}catch(std::exception const & e){
			std::cerr << "if"<< std::endl;
			std::cerr << e.what() << std::endl;
		}
	}else{
		pos = _width <= 22 ? 22 - _width : 0;
		try{

		snake[0].append("             ____", pos ,_width);
		snake[1].append("          .'`_ o `;__,", pos,_width);
		snake[2].append(".       .'.'` '---'  '", pos,_width);
		snake[3].append(".`-...-'.'", pos, _width);
		snake[4].append(" `-...-'", pos, _width);
		}catch(std::exception const & e){
			std::cerr << "else: "<< std::to_string(_width) << std::endl;
			std::cerr << e.what() << std::endl;
		}
	}
	// print banner
	mvprintw(size, 0,  snake[0].c_str());   //"--.   --..,_   _,.--.   --..,_   _,.--.   --..,_ ");
	mvprintw(size + 1, 0, snake[1].c_str());//"o  `;__. `'.:'`__ o  `;__. `'.:'`__ o  `;__. `'.:'"); //1 motif 18 chara
	mvprintw(size + 2, 0, snake[2].c_str());//"---'`  `  .'.:`. '---'`  `  .'.:`. '---'`  `  .'.:");
	mvprintw(size + 3, 0, snake[3].c_str());//"--....--'`.'  '.`'--....--'`.'  '.`'--....--'`.'  ");
	mvprintw(size + 4, 0, snake[4].c_str());//"--....--'`      `'--....--'`      `'--....--'`     ");

}

int NC_Displayer::display_menu(std::string *menu, int size){

	const char *choices[size];
	ITEM **my_items;
	MENU *my_menu;
	int c, n_choices, i;
	display_banner(size);
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	nonl(); // is needed to have access at KEY_ENTER (13)
	for (int cpt = 0; cpt < size; cpt++){
		choices[cpt] = menu[cpt].c_str();
	}




	n_choices = ARRAY_SIZE(choices);
	my_items = (ITEM **)calloc(n_choices + 1, sizeof(ITEM *));

	for(i = 0; i < n_choices; ++i)
		my_items[i] = new_item(choices[i], "");
	my_items[n_choices] = (ITEM *)NULL;

	my_menu = new_menu((ITEM **)my_items);

	mvprintw(_height, 0, "'q' or ESC to Exit");
	post_menu(my_menu);
	refresh();
	i = 0;
	while((c = getch()))
	{   switch(c)
	    {	case KEY_DOWN:
		        menu_driver(my_menu, REQ_DOWN_ITEM);
		        i++;
				break;
			case KEY_UP:
				menu_driver(my_menu, REQ_UP_ITEM);
				i--;
				break;
			case 13: //KEY_ENTER
				clear_window();
				return (i);
				break;
			case 27:
				clear_window();
				return (-1);
				break;
			case 'q':
				clear_window();
				return (-1);
				break;
		}
	}
	return 0;
}

void NC_Displayer::close(){
	endwin();
}

eKey NC_Displayer::get_input() const {

	int ch = getch();
	try {
		key_tab.at(ch);
	}catch(std::out_of_range const & e)
	{
		return key_undef;
	}
	return (key_tab.at(ch));
}

void NC_Displayer::insert_score(std::string score){
	if (score.size() > 0 && score.size() <= size_t(_width /2))
	{
		mvprintw(_height, (_width - 6),"%s", "SCORE:");
		mvprintw(_height + 1, _width - score.size(),"%s", score.c_str());
		refresh();
	}else
		throw Except_NC_Displayer(1, "ncurses library");
}

void NC_Displayer::insert_clock(std::string clock){
	if (clock.size() > 0 && clock.size() < 7){
		mvprintw(_height, 1, "%s", clock.c_str());
		refresh();
	}else
		throw Except_NC_Displayer(2, "ncurses library");
}


void NC_Displayer::delete_old_snake(){
	if (_old_snake.size() != 0)
	{
		for (std::list<SnakeObject>::iterator it = _old_snake.begin(); it != _old_snake.end(); ++it){
			if (it->getX() >= _width -2 || it->getY() >= _height -2)
				throw Except_NC_Displayer(3, "ncurses library");
			mvaddch(it->getY() + 1, it->getX() + 1 , ' ');
		}
	}
}

void NC_Displayer::insert_snake(std::list<SnakeObject> snake){
	if (snake.size() == 0)
		throw Except_NC_Displayer(5, "ncurses library");

	delete_old_snake();

	for (std::list<SnakeObject>::iterator it = snake.begin(); it != snake.end(); ++it){
		if (it->getX() >= _width -2 || it->getY() >= _height -2)
			throw Except_NC_Displayer(3, "ncurses library");
		mvaddch(it->getY() + 1, it->getX() + 1 , _snake_body[it->getAsset()]);
	}
	_old_snake = snake;
	refresh();
}

void NC_Displayer::delete_food(std::list<Point> food){
	for (std::list<Point>::iterator it = food.begin();it != food.end(); ++it){
		if (it->getX() >= _width -2 || it->getY() >= _height -2)
			throw Except_NC_Displayer(6, "ncurses library");
		mvaddch(it->getY() + 1, it->getX() + 1 , ' ');
	}
}

void NC_Displayer::insert_food(std::list<Point> food){

	 if (food.empty())
	 	return;
	if (_old_food.size() != 0)
		delete_food(_old_food);
	_old_food = food;
	for (std::list<Point>::iterator it = food.begin(); it != food.end(); ++it){
		if (it->getX() >= _width -2 || it->getY() >= _height -2)
			throw Except_NC_Displayer(7, "ncurses library");
		mvaddch(it->getY() + 1 , it->getX() + 1, '@');
	}
	refresh();
}

void NC_Displayer::insert_walls(std::list<Point> walls){
	border(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ') ;
	mvwvline(_win, 1, 0, '|', _height -1);
	mvwvline(_win, 1, _width -1, '|', _height -1);
	mvwhline(_win, 0, 1, '_', _width -2);
	mvwhline(_win, _height -1, 1, '_', _width -2);
	 if (walls.empty())
	 	return;
	for (std::list<Point>::iterator it = walls.begin(); it != walls.end(); ++it){
		if (it->getX() >= _width -2 || it->getY() >= _height -2)
			throw Except_NC_Displayer(10, "ncurses library");
		mvaddch(it->getY() + 1 , it->getX() + 1, '&');
	}

	refresh();
}

/*
** Exception class
*/

NC_Displayer::Except_NC_Displayer::Except_NC_Displayer(unsigned int n, std::string msg) : _n(n){
	_errors[0] = static_cast<std::string>("");
	_errors[1] = static_cast<std::string>(" : score string is empty or is too long to feet the window.");
	_errors[2] = static_cast<std::string>(" : clock string is empty or too long.");
	_errors[3] = static_cast<std::string>(" : snake coordinates are out of the map.");
	_errors[4] = static_cast<std::string>(" : _old_snake coordinates are out of the map.");
	_errors[5] = static_cast<std::string>(" : snake coordinates are empty.");
	_errors[6] = static_cast<std::string>(" : _old_food coordinates are out of the map.");
	_errors[7] = static_cast<std::string>(" : food coordinates are out of the map.");
	_errors[8] = static_cast<std::string>(" : food coordinates are empty.");
	_errors[9] = static_cast<std::string>(" : walls coordinates are empty.");
	_errors[10] = static_cast<std::string>(" : walls coordinates are out of the map.");
	_errors[n].insert(0, msg);
}

NC_Displayer::Except_NC_Displayer::Except_NC_Displayer(const Except_NC_Displayer & src){
	static_cast<void>(src);
}

NC_Displayer::Except_NC_Displayer::~Except_NC_Displayer() throw(){
}

NC_Displayer::Except_NC_Displayer & NC_Displayer::Except_NC_Displayer::operator=(const Except_NC_Displayer & rhs){

	static_cast<void>(rhs);
	return *this;
}

const char *NC_Displayer::Except_NC_Displayer::what() const throw(){
	return (_errors[_n].c_str());
}

std::ostream &			operator<<(std::ostream & o, NC_Displayer::Except_NC_Displayer const & i){
	o << i.what();
	return o;
}

