#ifndef NC_DISPLAYER_HPP
#define NC_DISPLAYER_HPP

#include "SnakeObject.hpp"
#include "IDisplayer.hpp"

#include <ncurses.h>
#include <menu.h>
#include <map>

#define NBR_ERRORS_POSSIBLE 11

class NC_Displayer : public IDisplayer{
	private:
		WINDOW					*_win;
		std::map <int, eKey>	key_tab;
		int 					_height;
		int 					_width;
		std::list<Point>		_old_food;
		char					_snake_body[14];
		std::list<SnakeObject>	_old_snake;

	public:
								NC_Displayer();
		virtual 				~NC_Displayer();
								NC_Displayer(const NC_Displayer & src);
		NC_Displayer 			&operator=(const NC_Displayer & src);
		void 					init(int height, int width);
		void					display_all();
		void 					close();
		void					clear_window();
		int 					display_menu(std::string *menu, int size);
		void 					insert_score(std::string score);
		void 					insert_clock(std::string clock);
		void 					insert_snake(std::list<SnakeObject> snake);
		void 					delete_old_snake();
		void 					insert_food(std::list<Point> food);
		void 					delete_food(std::list<Point> food);
		void 					insert_walls(std::list<Point> walls);
		void					display_banner(int size);
		eKey 					get_input() const;
		WINDOW					*get_win() const;
		std::map <int, eKey>	get_key_tab() const;
		int						get_height() const;
		int 					get_width() const;
		std::list<Point>		get_old_food() const;
		std::list<SnakeObject>	get_old_snake() const;




	class Except_NC_Displayer : public std::exception {
		private:
			std::string		_msg;
			std::string		_errors[NBR_ERRORS_POSSIBLE];
			unsigned int	_n;
							Except_NC_Displayer();
		public:
							Except_NC_Displayer(unsigned int n, std::string msg);
							Except_NC_Displayer(const Except_NC_Displayer & src);
		virtual				~Except_NC_Displayer() _NOEXCEPT;
		Except_NC_Displayer	&		operator=(const Except_NC_Displayer & rhs);
		virtual const char * what() const throw();
	};
};

std::ostream &			operator<<(std::ostream & o, NC_Displayer::Except_NC_Displayer const & i);

#endif