#include <AppleHandler.hpp>


AppleHandler::AppleHandler() {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler constructor called" << std::endl;
}

AppleHandler::AppleHandler(const AppleHandler & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler constructor called" << std::endl;
	const std::list<AppleObject> & l = src.getApples();
	for (std::list<AppleObject>::const_iterator i = l.begin(); i != l.end(); ++i)
		_apples.push_back(*i);
}

AppleHandler::~AppleHandler(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler destructor called" << std::endl;
}

AppleHandler &	AppleHandler::operator=(const AppleHandler & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler constructor called" << std::endl;
	const std::list<AppleObject> & l = src.getApples();
	for (std::list<AppleObject>::const_iterator i = l.begin(); i != l.end(); ++i)
		_apples.push_back(*i);
	return *this;
}

std::ostream &	operator<<(std::ostream & o, AppleHandler const & e){
	o << "AppleHandler containing " << e.getApples().size() << " apples ";
	for (std::list<AppleObject>::const_iterator apple = e.getApples().begin(); apple != e.getApples().end(); ++apple)
		o << "[" << apple->getX() << ":" << apple->getY() << "]";
	return o;
}

/*
	Methods
*/

void	AppleHandler::updateApples(){
	if (DEBUGG <= 3)
		_log << "[Debug:3] AppleHandler updating apples." << std::endl;
	for (std::list<AppleObject>::iterator apple = _apples.begin(); apple != _apples.end(); ++apple)
	{
		if ((*apple).isAlive()){
			if (DEBUGG <= 2)
				_log << "[Debug:2] AppleHandler removing apple" << *apple << std::endl;
			_apples.erase(apple);
		}
	}
}

const std::list<AppleObject> & AppleHandler::getApples() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler apples getter called" << std::endl;
	return _apples;
}

const std::list<Point> & AppleHandler::getApples(int) const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandler apples getter called" << std::endl;
	std::list<Point> *apples = new std::list<Point>;
	for (std::list<AppleObject>::const_iterator apple = _apples.begin(); apple != _apples.end(); ++apple)
			apples->push_back(*apple);
	return *apples;
}

void	AppleHandler::addApple(Point & p, unsigned int lifetime){
	if (DEBUGG <= 2)
		_log << "[Debug:2] AppleHandler adding an apple" << std::endl;
	try{
		if(isThereAnApple(p))
			throw AppleHandlerException("Apple already exist", __LINE__);
	}catch(AppleHandler::AppleHandlerException const &e){
		std::cerr << e.what() << std::endl;
		return ;
	}
	_apples.push_back(*new AppleObject (p,lifetime));
}

bool	AppleHandler::isThereAnApple(const Point & src){
	if (DEBUGG <= 2)
		_log << "[Debug:2] AppleHandler looking for an apple" << std::endl;
	for (std::list<AppleObject>::iterator apple = _apples.begin(); apple != _apples.end(); ++apple)
	{
		if (*apple == src)
			return true;
	}
	return false;
}

void	AppleHandler::eatTheApple(const Point & src){
	for (std::list<AppleObject>::iterator apple = _apples.begin(); apple != _apples.end(); ++apple)
	{
		if (*apple == src)
			_apples.erase(apple);
	}
	if (DEBUGG <= 4)
		_log << "[Debug:4] AppleHandler deleting an apple at " << src << ". " << _apples.size() << " apple(s) remaining." << std::endl;
}

/*
	AppleHandlerException
*/


AppleHandler::AppleHandlerException::AppleHandlerException() :  _msg("undefined error"), _line(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException constructor called" << std::endl;
}

AppleHandler::AppleHandlerException::AppleHandlerException(std::string msg) :  _msg(msg), _line(0) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException constructor called" << std::endl;
}

AppleHandler::AppleHandlerException::AppleHandlerException(std::string msg, unsigned int line) :  _msg(msg), _line(line) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException constructor called" << std::endl;
}

AppleHandler::AppleHandlerException::AppleHandlerException(const AppleHandlerException & src) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException copy constructor called" << std::endl;
	static_cast<void>(src);
}

AppleHandler::AppleHandlerException::~AppleHandlerException() _NOEXCEPT{
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException destructor called" << std::endl;
}

AppleHandler::AppleHandlerException & AppleHandler::AppleHandlerException::operator=(const AppleHandlerException & src) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleHandlerException operator = called" << std::endl;

	static_cast<void>(src);
	return *this;
}

const char *AppleHandler::AppleHandlerException::what() const throw(){
	std::string		str;
	if (_line > 0)
		str = "Error line(" + std::to_string(_line) + ") : " + _msg;
	else
		str = "Error : " + _msg;
	return (str.c_str());
}

std::ostream &	operator<<(std::ostream & o, AppleHandler::AppleHandlerException const & e){
	o << e.what();
	return o;
}