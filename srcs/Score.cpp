#include <Score.hpp>

Score::Score() : _score(0){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score constructor called" << std::endl;
}

Score::Score(const Score & src): _score(src.getScore()) {
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score constructor called" << std::endl;
}

Score::~Score(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score destructor called" << std::endl;
}

Score &		Score::operator=(const Score & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score constructor called" << std::endl;
	_score = src.getScore();
	return *this;
}

Score &		Score::operator+=(const int & n){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score constructor called" << std::endl;
	_score = _score + n;
	return *this;
}

Score &		Score::operator++(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score ++ operator called" << std::endl;
	_score++;
	return *this;
}

Score		Score::operator++(int){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score ++ operator called" << std::endl;
	Score tmp(*this);
	_score++;
	return tmp;
}

std::ostream &	operator<<(std::ostream & o, Score const & e){
	o << "Score(" << e.getScore() << ")";
	return o;
}

/*
	Methods
*/


const unsigned int &	Score::getScore() const {
	if (DEBUGG <= 1)
		_log << "[Debug:1] Score getter called" << std::endl;
	return _score;
}

std::string				Score::formatScore(int n) {
	if (DEBUGG <= 2)
		_log << "[Debug:2] Score format string called" << std::endl;
	std::string res = *new std::string;
	unsigned int score;
	score = _score;
	std::string letters("tbmk");
	char order = 0;

	while (score >= pow(10, n) + 1){
		letters.pop_back();
		score = score / 1000;
		order = letters.back();
	}
	res = std::to_string(score) + order;
	return res;
}
