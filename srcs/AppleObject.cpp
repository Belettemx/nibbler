#include <AppleObject.hpp>

AppleObject::AppleObject(){
	_lifeTime = 0;
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject constructor called" << std::endl;
}

AppleObject::AppleObject(const AppleObject & src) : Point(src), _lifeTime(src.getLifeTime()), _spawnTime(src.getSpawnTime()){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject constructor called" << std::endl;
}

AppleObject::AppleObject(Point & p, unsigned int lifeTime) : Point(p), _lifeTime(lifeTime){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject constructor called" << std::endl;
	_spawnTime = std::chrono::steady_clock::now();
}

AppleObject::AppleObject(unsigned int x, unsigned int y, unsigned int lifeTime) : Point(x, y), _lifeTime(lifeTime){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject constructor called" << std::endl;
	_spawnTime = std::chrono::steady_clock::now();
}

AppleObject::~AppleObject(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject destructor called" << std::endl;
}

AppleObject &	AppleObject::operator=(const AppleObject & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject constructor called" << std::endl;
	_lifeTime = src.getLifeTime();
	_spawnTime = src.getSpawnTime();
	_x = src.getX();
	_y = src.getY();
	return *this;
}

std::ostream &	operator<<(std::ostream & o, AppleObject const & e){
	std::chrono::steady_clock::time_point 			time;
	time = std::chrono::steady_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(time - e.getSpawnTime());
	o << "AppleObject(" << std::to_string(e.getX()) << ","+std::to_string(e.getY());
	o << ") life:" << (e.getLifeTime() - time_span.count()) << " s";
	return o;
}

/*
	Methods
*/

bool	AppleObject::isAlive(){
	if (DEBUGG <= 2)
		_log << "[Debug:2] AppleObject alive check : " << *this << std::endl;
	std::chrono::steady_clock::time_point 			time;
	time = std::chrono::steady_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double> >(time - _spawnTime);
	return time_span.count() >= _lifeTime;
}

const std::chrono::steady_clock::time_point	& AppleObject::getSpawnTime() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject spawnTime getter called" << std::endl;
	return _spawnTime;
}

const unsigned int &	AppleObject::getLifeTime() const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] AppleObject lifeTime getter called" << std::endl;
	return _lifeTime;
}