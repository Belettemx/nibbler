#include <Point.hpp>

Point::Point() : _x(0), _y(0){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point constructor called" << std::endl;
}

Point::Point(const Point & src): _x(src.getX()), _y(src.getY()){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point constructor called" << std::endl;
}

Point &		Point::operator=(const Point & src){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point constructor called" << std::endl;
	_x = src.getX();
	_y = src.getY();

	return *this;
}

Point::Point(int x, int y) : _x(x), _y(y){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point constructor called" << std::endl;
}

Point::~Point(){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point destructor called" << std::endl;
}

std::ostream &	operator<<(std::ostream & o, Point const & e){
	o << "Point(" << e.getX() << "," << e.getY() << ")";
	return o;
}

/*
	Methods
*/

const int	&	Point::getX() const {
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point x getter called" << std::endl;
	return _x;
}

const int	&	Point::getY() const {
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point y getter called" << std::endl;
	return _y;
}

void					Point::setX(int x){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point x setter called" << std::endl;
	_x = x;
}

void					Point::setY(int y){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point y setter called" << std::endl;
	_y = y;
}

bool		Point::operator==(const Point & r){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point equal operator called" << std::endl;
	return (_x == r.getX() && _y == r.getY());
}

bool		Point::operator!=(const Point & r){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point diff operator called" << std::endl;
	return (_x != r.getX() || _y != r.getY());
}

bool		Point::operator<(const Point & r) const{
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point diff operator called" << std::endl;
	if (_x < r.getX())
		return true;
	if (_x == r.getX() && _y < r.getY())
		return true;
	return false;
}

Point & 	Point::operator+(const Point & r){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point minus operator called" << std::endl;
	return *new Point (_x + r.getX(), _y + r.getY());
}

Point & 	Point::operator-(const Point & r){
	if (DEBUGG <= 1)
		_log << "[Debug:1] Point minus operator called" << std::endl;
	return *new Point (_x - r.getX(), _y - r.getY());
}
